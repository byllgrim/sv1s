# sv1s

Re-doing the SHIT-V processor core, but only 1 stage.


## Goals

RV32I:
* riscv-compliance
* RVFI

1364-2005:
* Iverilog
* Verilator

Synthesis:
* Yosys

Performance:
* abc
* icetime
* Embench
