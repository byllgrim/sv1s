`default_nettype none

`ifndef  RISCV_FORMAL
  `define  RISCV_FORMAL  1
`endif

module lec (
  input wire  clk,
  input wire  rst
);

  wire sv1s__rvfi_valid;
  // Metadata:
  wire [63 : 0] sv1s__rvfi_order;
  wire [31 : 0] sv1s__rvfi_insn;
  wire          sv1s__rvfi_trap;
  wire          sv1s__rvfi_halt;
  wire          sv1s__rvfi_intr;
  wire [ 1 : 0] sv1s__rvfi_mode;
  wire [ 1 : 0] sv1s__rvfi_ixl;
  // Register:
  wire [ 4 : 0] sv1s__rvfi_rs1_addr;
  wire [ 4 : 0] sv1s__rvfi_rs2_addr;
  wire [ 4 : 0] sv1s__rvfi_rd_addr;
  wire [31 : 0] sv1s__rvfi_rs1_rdata;
  wire [31 : 0] sv1s__rvfi_rs2_rdata;
  wire [31 : 0] sv1s__rvfi_rd_wdata;
  // PC:
  wire [31 : 0] sv1s__rvfi_pc_rdata;
  wire [31 : 0] sv1s__rvfi_pc_wdata;
  // Memory:
  wire [31 : 0] sv1s__rvfi_mem_addr;
  wire [ 3 : 0] sv1s__rvfi_mem_rmask;
  wire [ 3 : 0] sv1s__rvfi_mem_wmask;
  wire [31 : 0] sv1s__rvfi_mem_rdata;
  wire [31 : 0] sv1s__rvfi_mem_wdata;
  // Custom:
  wire sv1s__rvfi_cstm_unknowninstr;


  wire pico__rvfi_valid;
  // Metadata:
  wire [63 : 0] pico__rvfi_order;
  wire [31 : 0] pico__rvfi_insn;
  wire          pico__rvfi_trap;
  wire          pico__rvfi_halt;
  wire          pico__rvfi_intr;
  wire [ 1 : 0] pico__rvfi_mode;
  wire [ 1 : 0] pico__rvfi_ixl;
  // Register:
  wire [ 4 : 0] pico__rvfi_rs1_addr;
  wire [ 4 : 0] pico__rvfi_rs2_addr;
  wire [ 4 : 0] pico__rvfi_rd_addr;
  wire [31 : 0] pico__rvfi_rs1_rdata;
  wire [31 : 0] pico__rvfi_rs2_rdata;
  wire [31 : 0] pico__rvfi_rd_wdata;
  // PC:
  wire [31 : 0] pico__rvfi_pc_rdata;
  wire [31 : 0] pico__rvfi_pc_wdata;
  // Memory:
  wire [31 : 0] pico__rvfi_mem_addr;
  wire [ 3 : 0] pico__rvfi_mem_rmask;
  wire [ 3 : 0] pico__rvfi_mem_wmask;
  wire [31 : 0] pico__rvfi_mem_rdata;
  wire [31 : 0] pico__rvfi_mem_wdata;


  always @(posedge clk) begin
    // TODO remove these assumes
    asu_tmp_noexce:  assume (!pico__rvfi_trap);
    asu_tmp_nointr:  assume (!pico__rvfi_intr);
    asu_tmp_aligned: assume (sv1s__rvfi_mem_addr[1:0] == 0);
    asu_tmp_nomul:   assume (pico__rvfi_insn[6:0] != 7'b 0110011);  // M instrs
    asu_tmp_nocsr:   assume (
      !(
        // Zicsr instrs
        (pico__rvfi_insn[6:0] == 7'b 1110011)
        && (pico__rvfi_insn[14:12] != 0)
      )
    );
  end

  always @(posedge clk) begin
    if (!rst && pico__rvfi_valid) begin
      a_valid: assert (pico__rvfi_valid == sv1s__rvfi_valid);

      a_order: assert (pico__rvfi_order == sv1s__rvfi_order);
      a_insn:  assert (pico__rvfi_insn  == sv1s__rvfi_insn);
      a_trap:  assert (pico__rvfi_trap  == sv1s__rvfi_trap);
      a_halt:  assert (pico__rvfi_halt  == sv1s__rvfi_halt);
      a_intr:  assert (pico__rvfi_intr  == sv1s__rvfi_intr);
      a_mode:  assert (pico__rvfi_mode  == sv1s__rvfi_mode);
      a_ixl:   assert (pico__rvfi_ixl   == sv1s__rvfi_ixl);

      // TODO assert (pico__rvfi_rs1_addr  == sv1s__rvfi_rs1_addr);
      // TODO assert (pico__rvfi_rs2_addr  == sv1s__rvfi_rs2_addr);
      a_addr:  assert (pico__rvfi_rd_addr   == sv1s__rvfi_rd_addr);
      // TODO assert (pico__rvfi_rs1_rdata == sv1s__rvfi_rs1_rdata);
      // TODO assert (pico__rvfi_rs2_rdata == sv1s__rvfi_rs2_rdata);
      a_wdata: assert (pico__rvfi_rd_wdata  == sv1s__rvfi_rd_wdata);

      a_pcrdata: assert (pico__rvfi_pc_rdata == sv1s__rvfi_pc_rdata);
      a_pcwdata: assert (pico__rvfi_pc_wdata == sv1s__rvfi_pc_wdata);

      a_memaddr:  assert (pico__rvfi_mem_addr  == sv1s__rvfi_mem_addr);
      a_memrmask: assert (pico__rvfi_mem_rmask == sv1s__rvfi_mem_rmask);
      a_memwmask: assert (pico__rvfi_mem_wmask == sv1s__rvfi_mem_wmask);
      a_memrdata: assert (pico__rvfi_mem_rdata == sv1s__rvfi_mem_rdata);
      a_memwdata: assert (
        (pico__rvfi_mem_wdata & pico__rvfi_mem_wmask)
        ==
        (sv1s__rvfi_mem_wdata & sv1s__rvfi_mem_wmask)
      );
    end
  end


  sv1s_main sv1s (
    .sys_i_clk (clk),
    .sys_i_rst (rst),

    .dram_i_rdata (pico__rvfi_mem_rdata),
    .dram_o_bsel  (),
    .dram_o_addr  (),
    .dram_o_wdata (),
    .dram_o_wr    (),

    .rvfi_i_valid (pico__rvfi_valid),
    .rvfi_i_insn  (pico__rvfi_insn),

    .rvfi_valid (sv1s__rvfi_valid),

    .rvfi_order (sv1s__rvfi_order),
    .rvfi_insn  (sv1s__rvfi_insn),
    .rvfi_trap  (sv1s__rvfi_trap),
    .rvfi_halt  (sv1s__rvfi_halt),
    .rvfi_intr  (sv1s__rvfi_intr),
    .rvfi_mode  (sv1s__rvfi_mode),
    .rvfi_ixl   (sv1s__rvfi_ixl),

    .rvfi_rs1_addr  (sv1s__rvfi_rs1_addr),
    .rvfi_rs2_addr  (sv1s__rvfi_rs2_addr),
    .rvfi_rd_addr   (sv1s__rvfi_rd_addr),
    .rvfi_rs1_rdata (sv1s__rvfi_rs1_rdata),
    .rvfi_rs2_rdata (sv1s__rvfi_rs2_rdata),
    .rvfi_rd_wdata  (sv1s__rvfi_rd_wdata),

    .rvfi_pc_rdata (sv1s__rvfi_pc_rdata),
    .rvfi_pc_wdata (sv1s__rvfi_pc_wdata),

    .rvfi_mem_addr  (sv1s__rvfi_mem_addr),
    .rvfi_mem_rmask (sv1s__rvfi_mem_rmask),
    .rvfi_mem_wmask (sv1s__rvfi_mem_wmask),
    .rvfi_mem_rdata (sv1s__rvfi_mem_rdata),
    .rvfi_mem_wdata (sv1s__rvfi_mem_wdata),

    .rvfi_cstm_unknowninstr (sv1s__rvfi_cstm_unknowninstr)
  );


  picorv32 #(
    .ENABLE_COUNTERS   (0),
    .ENABLE_COUNTERS64 (0),
    .ENABLE_IRQ_QREGS  (0),
    .ENABLE_IRQ_TIMER  (0),
    .REGS_INIT_ZERO    (1)
  ) pico (
    .clk    (clk),
    .resetn (!rst),

    .mem_la_read  (mem_la_read),
    .mem_la_write (mem_la_write),
    .mem_rdata    (mem_rdata),
    .mem_ready    (mem_ready),
    .mem_valid    (mem_valid),
    .mem_addr     (),
    .mem_instr    (),
    .mem_la_addr  (),
    .mem_la_wdata (),
    .mem_la_wstrb (),
    .mem_wdata    (),
    .mem_wstrb    (),

    .pcpi_wr     (pcpi_wr),
    .pcpi_rd     (pcpi_rd),
    .pcpi_wait   (pcpi_wait),
    .pcpi_ready  (pcpi_ready),
    .irq         (irq),
    .eoi         (),
    .pcpi_insn   (),
    .pcpi_rs1    (),
    .pcpi_rs2    (),
    .pcpi_valid  (),
    .trace_data  (),
    .trace_valid (),
    .trap        (),

    .rvfi_valid   (pico__rvfi_valid),
    .rvfi_order   (pico__rvfi_order),
    .rvfi_insn    (pico__rvfi_insn),
    .rvfi_trap    (pico__rvfi_trap),
    .rvfi_halt    (pico__rvfi_halt),
    .rvfi_intr    (pico__rvfi_intr),
    .rvfi_mode    (pico__rvfi_mode),
    .rvfi_ixl     (pico__rvfi_ixl),

    .rvfi_rs1_addr  (pico__rvfi_rs1_addr),
    .rvfi_rs2_addr  (pico__rvfi_rs2_addr),
    .rvfi_rd_addr   (pico__rvfi_rd_addr),
    .rvfi_rs1_rdata (pico__rvfi_rs1_rdata),
    .rvfi_rs2_rdata (pico__rvfi_rs2_rdata),
    .rvfi_rd_wdata  (pico__rvfi_rd_wdata),

    .rvfi_pc_rdata  (pico__rvfi_pc_rdata),
    .rvfi_pc_wdata  (pico__rvfi_pc_wdata),

    .rvfi_mem_addr  (pico__rvfi_mem_addr),
    .rvfi_mem_rmask (pico__rvfi_mem_rmask),
    .rvfi_mem_wmask (pico__rvfi_mem_wmask),
    .rvfi_mem_rdata (pico__rvfi_mem_rdata),
    .rvfi_mem_wdata (pico__rvfi_mem_wdata),

    .rvfi_csr_mcycle_rmask (),
    .rvfi_csr_mcycle_wmask (),
    .rvfi_csr_mcycle_rdata (),
    .rvfi_csr_mcycle_wdata (),

    .rvfi_csr_minstret_rmask (),
    .rvfi_csr_minstret_wmask (),
    .rvfi_csr_minstret_rdata (),
    .rvfi_csr_minstret_wdata ()
  );

  wire        mem_la_read;
  wire        mem_la_write;
  wire        mem_valid;
  wire        mem_ready  = 'Z;  // TODO "Z" ok for this formal purpose?
  wire [31:0] mem_rdata  = 'Z;
  wire        pcpi_wr    = 'Z;
  wire [31:0] pcpi_rd    = 'Z;
  wire        pcpi_wait  = 'Z;
  wire        pcpi_ready = 'Z;
  wire [31:0] irq        = 'Z;

  always @(posedge clk) begin
    asu_picomem_compliant: assume (
      !(
        (mem_la_read || mem_la_write)
        && !(!mem_valid || mem_ready)
      )
    );
  end


  (*keep*) reg [7:0] cnt;
  always @(posedge clk) begin
    cnt <= (cnt < 20) ? (cnt + 8'd 1) : 8'd 0;  // TODO possible to reset without loop?
    if (cnt == 0) asu_rst: assume (rst);
  end


  always @(posedge clk) begin
    cover(!rst && sv1s__rvfi_valid);
  end

endmodule
