module obi_assert (
  input wire  sys_i_clk,
  input wire  sys_i_rst,

  input wire  obi_i_req,
  input wire  obi_i_gnt,
  input wire  obi_i_rvalid,

  input wire         ctrl_i_assume,
  input wire         ctrl_i_assert,
  input wire         ctrl_i_inflightcap,
  input wire [31:0]  ctrl_i_inflightmax
);

  reg  [31:0] misc_r_inflight;
  wire [31:0] misc_w_inflight;

  always @(posedge sys_i_clk) begin
    misc_r_inflight <= misc_w_inflight;
  end

  assign  misc_w_inflight =
    sys_i_rst
      ? 32'd 0
      : (obi_i_req && obi_i_gnt && !obi_i_rvalid)
        ? (misc_r_inflight + 1)
        : (!(obi_i_req && obi_i_gnt) && obi_i_rvalid)
          ? (misc_r_inflight - 1)
          : misc_r_inflight;

  wire  is_inflight_negative = ($signed(misc_w_inflight) < 0);
  wire  is_rvalid_early = (!misc_r_inflight && obi_i_rvalid);

  always @(*) begin
    a_ctrl: assert (ctrl_i_assume ^ ctrl_i_assert);

    if (ctrl_i_inflightcap) asu_inflight_cap: assume (misc_r_inflight <= ctrl_i_inflightmax);

    if (ctrl_i_assume) asu_inflight_pos: assume (!is_inflight_negative);
    if (ctrl_i_assert)   a_inflight_pos: assert (!is_inflight_negative);

    if (ctrl_i_assume) asu_rvalid: assume (!is_rvalid_early);
    if (ctrl_i_assert)   a_rvalid: assert (!is_rvalid_early);
  end

endmodule
