`default_nettype none


module tb;

  reg clk;
  reg rst;

  mcu mcu (
    .sys_i_clk (clk),
    .sys_i_rst (rst)
  );


  // Drive simulation

  initial begin
    $dumpfile("trace.vcd");
    $dumpvars();
  end

  initial begin
    clk = 1;
    forever #5 clk = !clk;
  end

  initial begin
    rst = 0;
    #1;
    rst = 1;
    #10;
    rst = 0;
  end

  initial begin
    repeat (128) @(posedge clk);
    $display("error: simulation timed out");
    $stop;
  end


  // Load hex file

  reg [1023:0] filename;

  initial begin
    if (!$value$plusargs("hex=%s", filename)) begin
      $display("error: no hexfile given");
      $stop;
    end

    $readmemh(filename, mcu.ram.mem, 0, mcu.ram.BYTES-1);
  end


  // Watch for exit code

  wire        sw = mcu.sv1s_top.sv1s_main.ins_sw_w;
  wire [31:0] addr =
    mcu.sv1s_top.sv1s_main.rf_regs_r[mcu.sv1s_top.sv1s_main.field_rs1_w]
    + mcu.sv1s_top.sv1s_main.field_imms_w;
  wire [31:0] data =
    mcu.sv1s_top.sv1s_main.rf_regs_r[mcu.sv1s_top.sv1s_main.field_rs2_w];
  // TODO no deep probing

  reg  [31:0] ret;

  always @(negedge clk) begin
    if (sw && (addr == 'h7FF)) begin
      ret = data;
      @(negedge clk);

      if (ret) begin
        $display("error: sw returned 0x%0X", ret);
        $stop;
      end else begin
        $finish;
      end
    end
  end

endmodule
