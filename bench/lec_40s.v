`default_nettype none


module lec_40s (
  input wire  clk,
  input wire  rst
);


  // Wires of Sv1s

  wire sv1s__rvfi_valid;
  // Metadata:
  wire [63 : 0] sv1s__rvfi_order;
  wire [31 : 0] sv1s__rvfi_insn;
  wire          sv1s__rvfi_trap;
  wire          sv1s__rvfi_halt;
  wire          sv1s__rvfi_intr;
  wire [ 1 : 0] sv1s__rvfi_mode;
  wire [ 1 : 0] sv1s__rvfi_ixl;
  // Register:
  wire [ 4 : 0] sv1s__rvfi_rs1_addr;
  wire [ 4 : 0] sv1s__rvfi_rs2_addr;
  wire [ 4 : 0] sv1s__rvfi_rd_addr;
  wire [31 : 0] sv1s__rvfi_rs1_rdata;
  wire [31 : 0] sv1s__rvfi_rs2_rdata;
  wire [31 : 0] sv1s__rvfi_rd_wdata;
  // PC:
  wire [31 : 0] sv1s__rvfi_pc_rdata;
  wire [31 : 0] sv1s__rvfi_pc_wdata;
  // Memory:
  wire [31 : 0] sv1s__rvfi_mem_addr;
  wire [ 3 : 0] sv1s__rvfi_mem_rmask;
  wire [ 3 : 0] sv1s__rvfi_mem_wmask;
  wire [31 : 0] sv1s__rvfi_mem_rdata;
  wire [31 : 0] sv1s__rvfi_mem_wdata;
  // Custom:
  wire sv1s__rvfi_cstm_unknowninstr;


  // Wires of E40s

  wire e40s__rvfi_valid;
  // Metadata:
  wire [63 : 0] e40s__rvfi_order;
  wire [31 : 0] e40s__rvfi_insn;
  wire          e40s__rvfi_trap;
  wire          e40s__rvfi_halt;
  wire          e40s__rvfi_intr;
  wire [ 1 : 0] e40s__rvfi_mode;
  wire [ 1 : 0] e40s__rvfi_ixl;
  // Register:
  wire [ 4 : 0] e40s__rvfi_rs1_addr;
  wire [ 4 : 0] e40s__rvfi_rs2_addr;
  wire [ 4 : 0] e40s__rvfi_rd_addr;
  wire [31 : 0] e40s__rvfi_rs1_rdata;
  wire [31 : 0] e40s__rvfi_rs2_rdata;
  wire [31 : 0] e40s__rvfi_rd_wdata;
  // PC:
  wire [31 : 0] e40s__rvfi_pc_rdata;
  wire [31 : 0] e40s__rvfi_pc_wdata;
  // Memory:
  wire [31 : 0] e40s__rvfi_mem_addr;
  wire [ 3 : 0] e40s__rvfi_mem_rmask;
  wire [ 3 : 0] e40s__rvfi_mem_wmask;
  wire [31 : 0] e40s__rvfi_mem_rdata;
  wire [31 : 0] e40s__rvfi_mem_wdata;
  // Custom:
  wire e40s__rvfi_cstm_dbgmode;


  // Wires of Obi Assertion

  wire  instr_w_req    = 'Z;
  wire  instr_w_gnt    = 'Z;
  wire  instr_w_rvalid = 'Z;
  wire  data_w_req     = 'Z;
  wire  data_w_gnt     = 'Z;
  wire  data_w_rvalid  = 'Z;


  // Signals of E40s Config

  reg          e40s_r_hasfetchenable;
  wire         e40s_w_fetchenable    = 'Z;
  reg  [31:0]  e40s_r_bootaddr;
  wire [31:0]  e40s_w_bootaddr       = 'Z;


  // Parameters of Config

  localparam logic  DEFAULT_RVFI_ORDER = 1'b 1;


  // Assumes of Temporary Constraints

  always @(posedge clk) begin
    if (!rst && e40s__rvfi_valid) begin
      // TODO remove these assumes
      asu_tmp_noexce:  assume (!e40s__rvfi_trap);
      asu_tmp_nointr:  assume (!e40s__rvfi_intr);
      asu_tmp_nodbg:   assume (!e40s__rvfi_cstm_dbgmode);
      asu_tmp_malign:  assume (e40s__rvfi_mem_addr[1:0] == 0);
      asu_tmp_ialignr: assume (e40s__rvfi_pc_rdata[1:0] == 2'b 00);
      asu_tmp_ialignw: assume (e40s__rvfi_pc_wdata[1:0] == 2'b 00);
      asu_tmp_mode:    assume (e40s__rvfi_mode == 2'b 11);
      asu_tmp_boot:    assume (e40s_w_bootaddr == 32'd 0);
      asu_tmp_nocompr: assume (e40s__rvfi_insn[1:0] == 2'b 11);
      asu_tmp_nosys:   assume (e40s__rvfi_insn[6:0] != 7'b 1110011);
      asu_tmp_nomul:   assume (e40s__rvfi_insn[6:0] != 7'b 0110011);  // M instrs
      asu_tmp_nocsr:   assume (
        !(
          // Zicsr instrs
          (e40s__rvfi_insn[6:0] == 7'b 1110011)  &&
          (e40s__rvfi_insn[14:12] != 0)
        )
      );
    end
  end


  // Asserts of Rvfi

  always @(posedge clk) begin
    if (!rst && e40s__rvfi_valid) begin
      a_valid: assert (e40s__rvfi_valid == sv1s__rvfi_valid);

      a_order: assert (e40s__rvfi_order == sv1s__rvfi_order);
      a_insn:  assert (e40s__rvfi_insn  == sv1s__rvfi_insn);
      a_trap:  assert (e40s__rvfi_trap  == sv1s__rvfi_trap);
      a_halt:  assert (e40s__rvfi_halt  == sv1s__rvfi_halt);
      a_intr:  assert (e40s__rvfi_intr  == sv1s__rvfi_intr);
      a_mode:  assert (e40s__rvfi_mode  == sv1s__rvfi_mode);
      a_ixl:   assert (e40s__rvfi_ixl   == sv1s__rvfi_ixl);

      // TODO assert (e40s__rvfi_rs1_addr  == sv1s__rvfi_rs1_addr);
      // TODO assert (e40s__rvfi_rs2_addr  == sv1s__rvfi_rs2_addr);
      a_addr:  assert (e40s__rvfi_rd_addr   == sv1s__rvfi_rd_addr);
      // TODO assert (e40s__rvfi_rs1_rdata == sv1s__rvfi_rs1_rdata);
      // TODO assert (e40s__rvfi_rs2_rdata == sv1s__rvfi_rs2_rdata);
      a_wdata: assert (e40s__rvfi_rd_wdata  == sv1s__rvfi_rd_wdata);

      a_pcrdata: assert (e40s__rvfi_pc_rdata == sv1s__rvfi_pc_rdata);
      a_pcwdata: assert (e40s__rvfi_pc_wdata == sv1s__rvfi_pc_wdata);

      a_memrmask: assert (e40s__rvfi_mem_rmask == sv1s__rvfi_mem_rmask);
      a_memwmask: assert (e40s__rvfi_mem_wmask == sv1s__rvfi_mem_wmask);
      a_memrdata: assert (e40s__rvfi_mem_rdata == sv1s__rvfi_mem_rdata);
      a_memwdata: assert (
        (e40s__rvfi_mem_wdata & e40s__rvfi_mem_wmask)
        ==
        (sv1s__rvfi_mem_wdata & sv1s__rvfi_mem_wmask)
      );
      if (e40s__rvfi_mem_rmask || e40s__rvfi_mem_wmask) begin
        a_memaddr:  assert (e40s__rvfi_mem_addr  == sv1s__rvfi_mem_addr);
      end
    end
  end


  // TODO check rd vs rs1/rs2 on new regfile model (sv1s_top?)
  // TODO same for pc
  /*
  reg [31:0] pc_r_next;
  reg [31:0] rf_regs_r [0:31];

  always @(posedge sys_i_clk) begin
    pc_r_next <= 0;

    if (sys_i_rst) begin: reset
      integer i;
      for (i = 0; i < 32; i = i + 1) begin
        rf_regs_r[i] = 0;
      end
    end else if (!sys_i_rst) begin: registering
      pc_r_next <= pc_w_next;

      if (rvfi_i_valid && rf_write_w && (field_rd_w != 0)) begin
        rf_regs_r[field_rd_w] <= rf_wdata_w;
      end
    end
  end
  */


  // Instantiation of Sv1s

  sv1s_main sv1s (
    .sys_i_clk (clk),
    .sys_i_rst (rst),

    .opt_i_defaultrvfiorder (DEFAULT_RVFI_ORDER),

    .dram_i_rdata (e40s__rvfi_mem_rdata),
    .dram_o_bsel  (),
    .dram_o_addr  (),
    .dram_o_wdata (),
    .dram_o_wr    (),

    .rvfi_i_insn       (e40s__rvfi_insn),
    .rvfi_i_pc_rdata   (e40s__rvfi_pc_rdata),
    .rvfi_i_rs1_rdata  (e40s__rvfi_rs1_rdata),
    .rvfi_i_rs2_rdata  (e40s__rvfi_rs2_rdata),
    .rvfi_i_valid      (e40s__rvfi_valid),

    .rvfi_valid (sv1s__rvfi_valid),

    .rvfi_order (sv1s__rvfi_order),
    .rvfi_insn  (sv1s__rvfi_insn),
    .rvfi_trap  (sv1s__rvfi_trap),
    .rvfi_halt  (sv1s__rvfi_halt),
    .rvfi_intr  (sv1s__rvfi_intr),
    .rvfi_mode  (sv1s__rvfi_mode),
    .rvfi_ixl   (sv1s__rvfi_ixl),

    .rvfi_rs1_addr  (sv1s__rvfi_rs1_addr),
    .rvfi_rs2_addr  (sv1s__rvfi_rs2_addr),
    .rvfi_rd_addr   (sv1s__rvfi_rd_addr),
    .rvfi_rs1_rdata (sv1s__rvfi_rs1_rdata),
    .rvfi_rs2_rdata (sv1s__rvfi_rs2_rdata),
    .rvfi_rd_wdata  (sv1s__rvfi_rd_wdata),

    .rvfi_pc_rdata (sv1s__rvfi_pc_rdata),
    .rvfi_pc_wdata (sv1s__rvfi_pc_wdata),

    .rvfi_mem_addr  (sv1s__rvfi_mem_addr),
    .rvfi_mem_rmask (sv1s__rvfi_mem_rmask),
    .rvfi_mem_wmask (sv1s__rvfi_mem_wmask),
    .rvfi_mem_rdata (sv1s__rvfi_mem_rdata),
    .rvfi_mem_wdata (sv1s__rvfi_mem_wdata),

    .rvfi_cstm_unknowninstr (sv1s__rvfi_cstm_unknowninstr)
  );


  // Instantiation of E40s

  cv32e40s_wrapper  e40s (
    .clk_i  (clk),
    .rst_ni (~rst),

    .rvfi_valid   (e40s__rvfi_valid),
    .rvfi_order   (e40s__rvfi_order),
    .rvfi_insn    (e40s__rvfi_insn),
    .rvfi_trap    (e40s__rvfi_trap),
    .rvfi_halt    (e40s__rvfi_halt),
    .rvfi_intr    (e40s__rvfi_intr),
    .rvfi_mode    (e40s__rvfi_mode),
    .rvfi_ixl     (e40s__rvfi_ixl),

    .rvfi_rs1_addr  (e40s__rvfi_rs1_addr),
    .rvfi_rs2_addr  (e40s__rvfi_rs2_addr),
    .rvfi_rd_addr   (e40s__rvfi_rd_addr),
    .rvfi_rs1_rdata (e40s__rvfi_rs1_rdata),
    .rvfi_rs2_rdata (e40s__rvfi_rs2_rdata),
    .rvfi_rd_wdata  (e40s__rvfi_rd_wdata),

    .rvfi_pc_rdata (e40s__rvfi_pc_rdata),
    .rvfi_pc_wdata (e40s__rvfi_pc_wdata),

    .rvfi_mem_addr  (e40s__rvfi_mem_addr),
    .rvfi_mem_rmask (e40s__rvfi_mem_rmask),
    .rvfi_mem_wmask (e40s__rvfi_mem_wmask),
    .rvfi_mem_rdata (e40s__rvfi_mem_rdata),
    .rvfi_mem_wdata (e40s__rvfi_mem_wdata),

    .instr_req_o    (instr_w_req),
    .instr_gnt_i    (instr_w_gnt),
    .instr_rvalid_i (instr_w_rvalid),
    .data_req_o     (data_w_req),
    .data_gnt_i     (data_w_gnt),
    .data_rvalid_i  (data_w_rvalid),

    .fetch_enable_i (e40s_w_fetchenable),
    .boot_addr_i    (e40s_w_bootaddr),

    .clic_irq_id_i       ('Z),
    .clic_irq_i          ('Z),
    .clic_irq_level_i    ('Z),
    .clic_irq_priv_i     ('Z),
    .clic_irq_shv_i      ('Z),
    .data_err_i          ('Z),
    .data_gntpar_i       ('Z),
    .data_rchk_i         ('Z),
    .data_rdata_i        ('Z),
    .data_rvalidpar_i    ('Z),
    .debug_req_i         ('Z),
    .dm_exception_addr_i ('Z),
    .dm_halt_addr_i      ('Z),
    .fencei_flush_ack_i  ('Z),
    .instr_err_i         ('Z),
    .instr_gntpar_i      ('Z),
    .instr_rchk_i        ('Z),
    .instr_rdata_i       ('Z),
    .instr_rvalidpar_i   ('Z),
    .irq_i               ('Z),
    .mhartid_i           ('Z),
    .mimpid_patch_i      ('Z),
    .mtvec_addr_i        ('Z),
    .scan_cg_en_i        ('Z),

    .alert_major_o      (),
    .alert_minor_o      (),
    .core_sleep_o       (),
    .data_achk_o        (),
    .data_addr_o        (),
    .data_be_o          (),
    .data_dbg_o         (),
    .data_memtype_o     (),
    .data_prot_o        (),
    .data_reqpar_o      (),
    .data_wdata_o       (),
    .data_we_o          (),
    .debug_halted_o     (),
    .debug_havereset_o  (),
    .debug_running_o    (),
    .fencei_flush_req_o (),
    .instr_achk_o       (),
    .instr_addr_o       (),
    .instr_dbg_o        (),
    .instr_memtype_o    (),
    .instr_prot_o       (),
    .instr_reqpar_o     (),
    .mcycle_o           ()
  );
  assign  e40s__rvfi_cstm_dbgmode = e40s.rvfi_i.rvfi_dbg_mode;


  // Asserts of Obi

  obi_assert  obi_assert_instr (
    .sys_i_clk (clk),
    .sys_i_rst (rst),

    .obi_i_req    (instr_w_req),
    .obi_i_gnt    (instr_w_gnt),
    .obi_i_rvalid (instr_w_rvalid),

    .ctrl_i_assume      ( 1'd 1),
    .ctrl_i_assert      ( 1'd 0),
    .ctrl_i_inflightcap ( 1'd 1),
    .ctrl_i_inflightmax (32'd 1)
  );

  obi_assert  obi_assert_data (
    .sys_i_clk (clk),
    .sys_i_rst (rst),

    .obi_i_req    (data_w_req),
    .obi_i_gnt    (data_w_gnt),
    .obi_i_rvalid (data_w_rvalid),

    .ctrl_i_assume      ( 1'd 1),
    .ctrl_i_assert      ( 1'd 0),
    .ctrl_i_inflightcap ( 1'd 1),
    .ctrl_i_inflightmax (32'd 1)
  );


  // Control of E40s Config

  always @(posedge clk) begin
    e40s_r_bootaddr <= e40s_w_bootaddr;

    if (rst) begin
      e40s_r_hasfetchenable <= 0;
    end else begin
      if (e40s_w_fetchenable) e40s_r_hasfetchenable <= 1;

      if (e40s_r_hasfetchenable) asu_bootaddr: assume (e40s_w_bootaddr == e40s_r_bootaddr);
    end
  end


  // Helper Asserts of Rvfi Order

  reg [63:0]  order_r_preve40s;
  reg [63:0]  order_r_prevsv1s;
  reg [63:0]  order_r_clockedsv1s;
  reg         valid_r_clockedsv1s;

  always @(posedge clk) begin
    if (e40s__rvfi_valid) begin
      order_r_preve40s <= e40s__rvfi_order;
      order_r_prevsv1s <= sv1s__rvfi_order;

      if (e40s__rvfi_order > 1) begin
        a_order_increasing_e40s: assert (e40s__rvfi_order  >  order_r_preve40s);
        // Not efficient?  a_order_increasing_sv1s: assert (sv1s__rvfi_order  >  order_r_prevsv1s);
        a_order_plusone_e40s:    assert (e40s__rvfi_order == (order_r_preve40s + 1));
        a_order_plusone_sv1s:    assert (sv1s__rvfi_order == (order_r_prevsv1s + 1));
      end
    end

    order_r_clockedsv1s <= sv1s__rvfi_order;
    valid_r_clockedsv1s <= sv1s__rvfi_valid;
    if (!valid_r_clockedsv1s) begin
      a_order_stable_sv1s: assert (sv1s__rvfi_order == order_r_clockedsv1s);
    end
  end

endmodule
