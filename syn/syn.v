`default_nettype none

module syn (
  input wire sys_clk_i,
  input wire sys_rst_i,

  // Ram Instruction Interface
  input  wire [31:0] iram_rdata_i,
  input  wire        iram_valid_i,
  output wire [31:0] iram_addr_o,

  // Ram Data Interface
  input  wire [31:0] dram_rdata_i,
  output wire [ 3:0] dram_bsel_o,
  output wire [31:0] dram_addr_o,
  output wire [31:0] dram_wdata_o,
  output wire        dram_wr_o
);

  sv1s_top sv1s_top (
    .sys_clk_i (sys_clk_i),
    .sys_rst_i (sys_rst_i),

    .iram_rdata_i (iram_rdata_i),
    .iram_valid_i (iram_valid_i),

    .iram_addr_o (iram_addr_o),

    .dram_rdata_i (dram_rdata_i),

    .dram_addr_o  (dram_addr_o),
    .dram_bsel_o  (dram_bsel_o),
    .dram_wdata_o (dram_wdata_o),
    .dram_wr_o    (dram_wr_o)
  );

endmodule
