.global _start
_start:
	beq	x0, x0, halt

fail:
	addi	x31, x0, 1
	jal	x0, fail2

halt:
	addi	x30, x0, 0x7FF
	sw	x31, 0(x30)

fail2:
	nop
