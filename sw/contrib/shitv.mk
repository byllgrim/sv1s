default:
	@echo read the makefile

shitv:
	git clone https://gitlab.com/byllgrim/shitv.git

comp:
	mkdir -p asm
	find shitv/ | grep '\.S' | xargs cp -t asm
	make compall LIST="$$(ls asm | tr '\n' ' ')"

compall:
	mkdir -p elf
	mkdir -p hex
	for a in $(LIST); do \
		riscv64-unknown-elf-gcc \
			-nostartfiles -nodefaultlibs \
			-march=rv32i -mabi=ilp32 \
			-T /dev/null \
			-o elf/$$a.elf \
			asm/$$a; \
		riscv64-unknown-elf-objcopy \
			-O verilog \
			elf/$$a.elf \
			hex/$$a.hex; \
	done

run:
	make runall LIST="$$(ls hex | tr '\n' ' ')"

runall:
	@for h in $(LIST); do \
		vvp -N ../sv1s.vvp +hex=hex/$$h > /dev/null; \
		if [ $$? -ne 0 ]; then                 \
			echo -n "FAIL ";               \
		else                                   \
			echo -n "OK   ";               \
		fi;                                    \
		echo $$h; \
	done

runtest:
	vvp -N ../sv1s.vvp +hex=hex/$(TEST).S.hex
