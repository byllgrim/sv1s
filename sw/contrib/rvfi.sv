`default_nettype none

// Note: These defines patches a bug in riscv-formal
`define  rvformal_rand_reg        rand reg
`define  rvformal_const_rand_reg  rand const reg

module rvfi_wrapper (
  input  clock,
  input  reset,

  `RVFI_OUTPUTS
);

  // Inputs

  (* keep *) `rvformal_rand_reg [31:0] iram_r_dat;
  (* keep *) wire               [31:0] iram_w_adr;
  (* keep *) wire                      iram_w_cyc;
  (* keep *) wire                      iram_w_stb;

  (* keep *) `rvformal_rand_reg [31:0] dram_rdata_r;
  (* keep *) reg                       dram_wr_w;


  // DUT

  sv1s_top dut (
    .sys_i_clk (clock),
    .sys_i_rst (reset),

    .iram_i_dat (iram_r_dat),
    .iram_o_adr (iram_w_adr),

    .dram_i_rdata (dram_rdata_r),
    .dram_o_wr    (dram_wr_w),


    `RVFI_CONN
  );

endmodule
