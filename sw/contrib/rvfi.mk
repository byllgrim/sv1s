default:
	@echo read the makefile

rvfi:
	git clone https://github.com/SymbioticEDA/riscv-formal.git rvfi

generate:
	mkdir -p rvfi/cores/sv1s
	cp rvfi.cfg rvfi/cores/sv1s/checks.cfg
	cd rvfi/cores/sv1s/ \
		&& python3 ../../checks/genchecks.py
	cp rvfi.sv rvfi/cores/sv1s/wrapper.sv
	cp -t rvfi/cores/sv1s/ ../../rtl/*.v
