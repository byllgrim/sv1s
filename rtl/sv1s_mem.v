`default_nettype none

module sv1s_mem (
  input wire sys_i_clk,
  input wire sys_i_rst,

  input wire [31:0] rvfi_i_pc_wdata,
  input wire        rvfi_i_valid,

  output reg [31:0] iram_o_adr,
  output reg        iram_o_stb
);

  reg iram_r_cyc;

  always @(posedge sys_i_clk) begin
    ctrl_r_resat <= ctrl_w_resat;

    iram_r_cyc <= 0;
    iram_o_adr <= 0;
    iram_o_stb <= 0;

    if (!sys_i_rst) begin
      iram_o_adr <= rvfi_i_pc_wdata;
      iram_o_stb <= ctrl_r_resat || rvfi_i_valid;
      iram_r_cyc <= 1;
    end
  end


  // Control first cycle

  reg  ctrl_r_resat;
  wire ctrl_w_resat =
    sys_i_rst
      ? 1
      : (iram_r_cyc && iram_o_stb)
        ? 0
        : ctrl_r_resat;

endmodule
