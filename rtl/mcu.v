`default_nettype none

module mcu (
  input wire sys_i_clk,
  input wire sys_i_rst
);

  wire [ 3:0] dram_w_bsel;
  wire [31:0] dram_w_addr;
  wire [31:0] dram_w_rdata;
  wire [31:0] dram_w_wdata;
  wire        dram_w_wr;

  wire [31:0] iram_w_adr;
  wire [31:0] iram_w_dat;

  sv1s_top sv1s_top (
    .sys_i_clk (sys_i_clk),
    .sys_i_rst (sys_i_rst),

    .dram_i_rdata (dram_w_rdata),

    .dram_o_addr  (dram_w_addr),
    .dram_o_bsel  (dram_w_bsel),
    .dram_o_wdata (dram_w_wdata),
    .dram_o_wr    (dram_w_wr),

    .iram_i_dat (iram_w_dat),
    .iram_o_adr (iram_w_adr),

    .rvfi_valid     (),
    .rvfi_order     (),
    .rvfi_insn      (),
    .rvfi_trap      (),
    .rvfi_halt      (),
    .rvfi_intr      (),
    .rvfi_mode      (),
    .rvfi_ixl       (),
    .rvfi_rs1_addr  (),
    .rvfi_rs2_addr  (),
    .rvfi_rs1_rdata (),
    .rvfi_rs2_rdata (),
    .rvfi_rd_addr   (),
    .rvfi_rd_wdata  (),
    .rvfi_pc_rdata  (),
    .rvfi_pc_wdata  (),
    .rvfi_mem_addr  (),
    .rvfi_mem_rmask (),
    .rvfi_mem_wmask (),
    .rvfi_mem_rdata (),
    .rvfi_mem_wdata ()
  );

  ram ram (
    .clk (sys_i_clk),
    .rst (sys_i_rst),

    .dram_i_addr  (dram_w_addr),
    .dram_i_bsel  (dram_w_bsel),
    .dram_i_wdata (dram_w_wdata),
    .dram_i_wr    (dram_w_wr),

    .dram_o_rdata (dram_w_rdata),

    .iram_i_adr (iram_w_adr),
    .iram_o_dat (iram_w_dat)
  );

endmodule
