`default_nettype none

module sv1s_top (
  input wire sys_i_clk,
  input wire sys_i_rst,

  input  wire [31:0] iram_i_dat,
  output wire [31:0] iram_o_adr,

  input  wire [31:0] dram_i_rdata,
  output wire [ 3:0] dram_o_bsel,
  output wire [31:0] dram_o_addr,
  output wire [31:0] dram_o_wdata,
  output wire        dram_o_wr,

  output wire [ 1:0] rvfi_ixl,
  output wire [ 1:0] rvfi_mode,
  output wire [ 3:0] rvfi_mem_rmask,
  output wire [ 3:0] rvfi_mem_wmask,
  output wire [31:0] rvfi_insn,
  output wire [31:0] rvfi_mem_addr,
  output wire [31:0] rvfi_mem_rdata,
  output wire [31:0] rvfi_mem_wdata,
  output wire [31:0] rvfi_pc_rdata,
  output wire [31:0] rvfi_pc_wdata,
  output wire [31:0] rvfi_rd_wdata,
  output wire [31:0] rvfi_rs1_rdata,
  output wire [31:0] rvfi_rs2_rdata,
  output wire [ 4:0] rvfi_rd_addr,
  output wire [ 4:0] rvfi_rs1_addr,
  output wire [ 4:0] rvfi_rs2_addr,
  output wire [63:0] rvfi_order,
  output wire        rvfi_halt,
  output wire        rvfi_intr,
  output wire        rvfi_trap,
  output wire        rvfi_valid
);

  wire        iram_w_stb;
  wire [31:0] iram_w_dat = iram_w_stb ? iram_i_dat : 0;

  sv1s_main sv1s_main (
    .sys_i_clk (sys_i_clk),
    .sys_i_rst (sys_i_rst),

    .dram_i_rdata (dram_i_rdata),

    .rvfi_i_insn  (iram_w_dat),
    .rvfi_i_valid (iram_w_stb),

    .dram_o_bsel  (dram_o_bsel),
    .dram_o_addr  (dram_o_addr),
    .dram_o_wdata (dram_o_wdata),
    .dram_o_wr    (dram_o_wr),

    .rvfi_ixl               (rvfi_ixl),
    .rvfi_mode              (rvfi_mode),
    .rvfi_mem_rmask         (rvfi_mem_rmask),
    .rvfi_mem_wmask         (rvfi_mem_wmask),
    .rvfi_insn              (rvfi_insn),
    .rvfi_mem_addr          (rvfi_mem_addr),
    .rvfi_mem_rdata         (rvfi_mem_rdata),
    .rvfi_mem_wdata         (rvfi_mem_wdata),
    .rvfi_pc_rdata          (rvfi_pc_rdata),
    .rvfi_pc_wdata          (rvfi_pc_wdata),
    .rvfi_rd_wdata          (rvfi_rd_wdata),
    .rvfi_rs1_rdata         (rvfi_rs1_rdata),
    .rvfi_rs2_rdata         (rvfi_rs2_rdata),
    .rvfi_rd_addr           (rvfi_rd_addr),
    .rvfi_rs1_addr          (rvfi_rs1_addr),
    .rvfi_rs2_addr          (rvfi_rs2_addr),
    .rvfi_order             (rvfi_order),
    .rvfi_halt              (rvfi_halt),
    .rvfi_intr              (rvfi_intr),
    .rvfi_trap              (rvfi_trap),
    .rvfi_valid             (rvfi_valid),
    .rvfi_cstm_unknowninstr ()
  );

  sv1s_mem sv1s_mem (
    .sys_i_clk (sys_i_clk),
    .sys_i_rst (sys_i_rst),

    .rvfi_i_pc_wdata (rvfi_pc_wdata),
    .rvfi_i_valid    (rvfi_valid),

    .iram_o_adr (iram_o_adr),
    .iram_o_stb (iram_w_stb)
  );

endmodule
