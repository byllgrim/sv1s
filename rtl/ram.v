`default_nettype none

module ram (
  input wire clk,
  input wire rst,

  input  wire [31:0] iram_i_adr,
  output wire [31:0] iram_o_dat,

  input  wire [ 3:0] dram_i_bsel,
  input  wire [31:0] dram_i_addr,
  input  wire [31:0] dram_i_wdata,
  input  wire        dram_i_wr,
  output wire [31:0] dram_o_rdata
);

  localparam ADDRWIDTH = 16;
  localparam BYTES = 2 ** ADDRWIDTH;

  reg [ 7:0] mem [0:BYTES-1];


  // Set outputs

  assign iram_o_dat =
    {mem[iram_i_adr+3], mem[iram_i_adr+2],
      mem[iram_i_adr+1], mem[iram_i_adr+0]};

  assign dram_o_rdata =
    {mem[dram_i_addr+3], mem[dram_i_addr+2],
      mem[dram_i_addr+1], mem[dram_i_addr+0]};

  always @(posedge clk) begin
    if (!rst && dram_i_wr) begin
/* TODO
      mem[dram_i_addr + 0] <= dram_i_bsel[0] ? dram_i_wdata[ 7: 0] : 0;
      mem[dram_i_addr + 1] <= dram_i_bsel[1] ? dram_i_wdata[15: 8] : 0;
      mem[dram_i_addr + 2] <= dram_i_bsel[2] ? dram_i_wdata[23:16] : 0;
      mem[dram_i_addr + 3] <= dram_i_bsel[3] ? dram_i_wdata[31:24] : 0;
*/

      if (dram_i_bsel[0]) mem[dram_i_addr + 0] <= dram_i_wdata[ 7: 0];
      if (dram_i_bsel[1]) mem[dram_i_addr + 1] <= dram_i_wdata[15: 8];
      if (dram_i_bsel[2]) mem[dram_i_addr + 2] <= dram_i_wdata[23:16];
      if (dram_i_bsel[3]) mem[dram_i_addr + 3] <= dram_i_wdata[31:24];
    end
  end

endmodule
