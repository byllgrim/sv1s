`default_nettype none

module sv1s_main (
  input wire sys_i_clk,
  input wire sys_i_rst,

  input wire [31:0] rvfi_i_insn,
  input wire [31:0] rvfi_i_pc_rdata,
  input wire [31:0] rvfi_i_rs1_rdata,
  input wire [31:0] rvfi_i_rs2_rdata,
  input wire        rvfi_i_valid,

  input wire  opt_i_defaultrvfiorder,

  input  wire [31:0] dram_i_rdata,
  output wire [ 3:0] dram_o_bsel,
  output wire [31:0] dram_o_addr,
  output wire [31:0] dram_o_wdata,
  output wire        dram_o_wr,

  output wire [ 1:0] rvfi_ixl,
  output wire [ 1:0] rvfi_mode,
  output wire [ 3:0] rvfi_mem_rmask,
  output wire [ 3:0] rvfi_mem_wmask,
  output wire [31:0] rvfi_insn,
  output wire [31:0] rvfi_mem_addr,
  output wire [31:0] rvfi_mem_rdata,
  output wire [31:0] rvfi_mem_wdata,
  output wire [31:0] rvfi_pc_rdata,
  output wire [31:0] rvfi_pc_wdata,
  output wire [31:0] rvfi_rd_wdata,
  output wire [31:0] rvfi_rs1_rdata,
  output wire [31:0] rvfi_rs2_rdata,
  output wire [ 4:0] rvfi_rd_addr,
  output wire [ 4:0] rvfi_rs1_addr,
  output wire [ 4:0] rvfi_rs2_addr,
  output wire [63:0] rvfi_order,
  output wire        rvfi_halt,
  output wire        rvfi_intr,
  output wire        rvfi_trap,
  output wire        rvfi_valid,
  output wire        rvfi_cstm_unknowninstr
);

  // Set dram outputs

  assign dram_o_addr  = dram_w_addr;
  assign dram_o_bsel  = dram_w_bsel;
  assign dram_o_wdata = dram_w_wdata;
  assign dram_o_wr    = dram_w_wr;


  // Decode

  // Fields of the instruction
  wire [ 6:0] field_op_w     = rvfi_i_insn[6:0];
  wire [ 2:0] field_funct3_w = rvfi_i_insn[14:12];
  wire [ 6:0] field_funct7_w = rvfi_i_insn[31:25];
  wire [31:0] field_immu_w   = {rvfi_i_insn[31:12], 12'd0};
  wire [ 4:0] field_rs1_w    = rvfi_i_insn[19:15];
  wire [ 4:0] field_rs2_w    = rvfi_i_insn[24:20];
  wire [ 4:0] field_rd_w     = rvfi_i_insn[11:7];
  wire [ 4:0] field_shamt_w  = field_rs2_w;
  wire [31:0] field_imms_w = {
    {21{rvfi_i_insn[31]}},
    rvfi_i_insn[30:25],
    rvfi_i_insn[11:7]
    };
  wire [31:0] field_immi_w = {
    {21{rvfi_i_insn[31]}},
    rvfi_i_insn[30:20]
    };
  wire [31:0] field_immj_w = {
    {12{rvfi_i_insn[31]}},
    rvfi_i_insn[19:12],
    rvfi_i_insn[20],
    rvfi_i_insn[30:21], 1'b0
    };
  wire [31:0] field_immb_w = {
    {20{rvfi_i_insn[31]}},
    rvfi_i_insn[7],
    rvfi_i_insn[30:25],
    rvfi_i_insn[11:8], 1'b0
    };

  // Operands
  wire op_auipc_w  = (field_op_w == 'b00_101_11);
  wire op_branch_w = (field_op_w == 'b11_000_11);
  wire op_jal_w    = (field_op_w == 'b11_011_11);
  wire op_jalr_w   = (field_op_w == 'b11_001_11);
  wire op_load_w   = (field_op_w == 'b00_000_11);
  wire op_lui_w    = (field_op_w == 'b01_101_11);
  wire op_opimm_w  = (field_op_w == 'b00_100_11);
  wire op_opreg_w  = (field_op_w == 'b01_100_11);
  wire op_store_w  = (field_op_w == 'b01_000_11);

  // Instructions du Immediate Op
  wire ins_addi_w  = (op_opimm_w && (field_funct3_w == 'b000));
  wire ins_slti_w  = (op_opimm_w && (field_funct3_w == 'b010));
  wire ins_sltiu_w = (op_opimm_w && (field_funct3_w == 'b011));
  wire ins_xori_w  = (op_opimm_w && (field_funct3_w == 'b100));
  wire ins_ori_w   = (op_opimm_w && (field_funct3_w == 'b110));
  wire ins_andi_w  = (op_opimm_w && (field_funct3_w == 'b111));
  wire ins_lui_w   = op_lui_w;
  wire ins_auipc_w = op_auipc_w;
  wire ins_slli_w  =
    op_opimm_w && (field_funct3_w == 'b001) && (field_funct7_w == 7'd0);
  wire ins_srli_w  =
    op_opimm_w && (field_funct3_w == 'b101) && (field_funct7_w == 7'd0);
  wire ins_srai_w  =
    op_opimm_w
    && (field_funct3_w == 'b101)
    && (field_funct7_w == 7'd32);

  // Instructions du Register-Register
  wire ins_add_w = (op_opreg_w && (field_funct3_w == 'b000)
                    && (field_funct7_w == 'd0));
  wire ins_or_w = (op_opreg_w && (field_funct3_w == 'b110)
                   && (field_funct7_w == 'd0));
  wire ins_xor_w = (op_opreg_w && (field_funct3_w == 'b100)
                    && (field_funct7_w == 'd0));
  wire ins_and_w = (op_opreg_w && (field_funct3_w == 'b111)
                    && (field_funct7_w == 'd0));
  wire ins_sub_w = (op_opreg_w && (field_funct3_w == 'b000)
                    && (field_funct7_w == 'b0100000));
  wire ins_sll_w = (op_opreg_w && (field_funct3_w == 'b001)
                    && (field_funct7_w == 'd0));
  wire ins_srl_w = (op_opreg_w && (field_funct3_w == 'b101)
                    && (field_funct7_w == 'd0));
  wire ins_sra_w = (op_opreg_w && (field_funct3_w == 'b101)
                    && (field_funct7_w == 'b0100000));
  wire ins_slt_w = (op_opreg_w && (field_funct3_w == 'b010)
                    && (field_funct7_w == 'd0));
  wire ins_sltu_w = (op_opreg_w && (field_funct3_w == 'b011)
                     && (field_funct7_w == 'd0));

  // Instructions du Load/Store
  wire ins_lb_w  = (op_load_w  && (field_funct3_w == 'b000));
  wire ins_lh_w  = (op_load_w  && (field_funct3_w == 'b001));
  wire ins_lw_w  = (op_load_w  && (field_funct3_w == 'b010));
  wire ins_lbu_w = (op_load_w  && (field_funct3_w == 'b100));
  wire ins_lhu_w = (op_load_w  && (field_funct3_w == 'b101));
  wire ins_sb_w  = (op_store_w && (field_funct3_w == 'b000));
  wire ins_sh_w  = (op_store_w && (field_funct3_w == 'b001));
  wire ins_sw_w  = (op_store_w && (field_funct3_w == 'b010));

  // Instructions du Control Transfer
  wire ins_beq_w  = (op_branch_w && (field_funct3_w == 'b000));
  wire ins_bne_w  = (op_branch_w && (field_funct3_w == 'b001));
  wire ins_blt_w  = (op_branch_w && (field_funct3_w == 'b100));
  wire ins_bge_w  = (op_branch_w && (field_funct3_w == 'b101));
  wire ins_bltu_w = (op_branch_w && (field_funct3_w == 'b110));
  wire ins_bgeu_w = (op_branch_w && (field_funct3_w == 'b111));
  wire ins_jal_w  = op_jal_w;
  wire ins_jalr_w = op_jalr_w;


  // Control Logic

  // Jumping
  wire ctrl_jump_w =
    (ins_beq_w && (rvfi_i_rs1_rdata == rvfi_i_rs2_rdata))
    || (ins_bne_w && (rvfi_i_rs1_rdata != rvfi_i_rs2_rdata))
    || (ins_blt_w && ($signed(rvfi_i_rs1_rdata) < $signed(rvfi_i_rs2_rdata)))
    || (ins_bge_w && ($signed(rvfi_i_rs1_rdata) >= $signed(rvfi_i_rs2_rdata)))
    || (ins_bltu_w && (rvfi_i_rs1_rdata < rvfi_i_rs2_rdata))
    || (ins_bgeu_w && (rvfi_i_rs1_rdata >= rvfi_i_rs2_rdata))
    || (ins_jal_w)
    || (ins_jalr_w);
  wire ctrl_branch_w =
    (ins_beq_w || ins_bne_w || ins_blt_w || ins_bge_w || ins_bltu_w
     || ins_bgeu_w);


  // Compute Register File Data/Controls

  wire [31:0] rf_wdata_w =
    (field_rd_w == 0) ? 0 :
    ins_addi_w  ? (rvfi_i_rs1_rdata + field_immi_w) :
    ins_slti_w  ? {31'd0,
                  ($signed(rvfi_i_rs1_rdata) < $signed(field_immi_w))} :
    ins_sltiu_w ? {31'd0, (rvfi_i_rs1_rdata < field_immi_w)} :
    ins_xori_w  ? (rvfi_i_rs1_rdata ^ field_immi_w) :
    ins_ori_w   ? (rvfi_i_rs1_rdata | field_immi_w) :
    ins_andi_w  ? (rvfi_i_rs1_rdata & field_immi_w) :
    ins_slli_w  ? (rvfi_i_rs1_rdata << field_shamt_w) :
    ins_srli_w  ? (rvfi_i_rs1_rdata >> field_shamt_w) :
    ins_srai_w  ? $signed($signed(rvfi_i_rs1_rdata) >>> field_shamt_w) :
    ins_lui_w   ? field_immu_w :
    ins_auipc_w ? (field_immu_w + rvfi_i_pc_rdata) :
    ins_lw_w    ? dram_i_rdata :
    ins_lh_w    ? {{17{dram_i_rdata[15]}}, dram_i_rdata[14:0]} :
    ins_lb_w    ? {{25{dram_i_rdata[7]}}, dram_i_rdata[6:0]} :
    ins_lhu_w   ? {16'd0, dram_i_rdata[15:0]} :
    ins_lbu_w   ? {24'd0, dram_i_rdata[7:0]} :
    ins_add_w   ? (rvfi_i_rs1_rdata + rvfi_i_rs2_rdata) :
    ins_or_w    ? (rvfi_i_rs1_rdata | rvfi_i_rs2_rdata) :
    ins_xor_w   ? (rvfi_i_rs1_rdata ^ rvfi_i_rs2_rdata) :
    ins_and_w   ? (rvfi_i_rs1_rdata & rvfi_i_rs2_rdata) :
    ins_sub_w   ? (rvfi_i_rs1_rdata - rvfi_i_rs2_rdata) :
    ins_sll_w   ? (rvfi_i_rs1_rdata << rvfi_i_rs2_rdata[4:0]) :
    ins_srl_w   ? (rvfi_i_rs1_rdata >> rvfi_i_rs2_rdata[4:0]) :
    ins_sra_w   ? $signed($signed(rvfi_i_rs1_rdata) >>> rvfi_i_rs2_rdata[4:0]):
    ins_slt_w   ? {31'd0, ($signed(rvfi_i_rs1_rdata) < $signed(rvfi_i_rs2_rdata))} :
    ins_sltu_w  ? {31'd0, (rvfi_i_rs1_rdata < rvfi_i_rs2_rdata)} :
    ins_jal_w   ? pc_incr_w :
    ins_jalr_w  ? pc_incr_w :
    0;
  wire rf_write_w =
    ins_addi_w || ins_slti_w || ins_sltiu_w || ins_xori_w
    || ins_ori_w || ins_andi_w || ins_slli_w || ins_srli_w
    || ins_srai_w || ins_lui_w || ins_auipc_w || ins_add_w
    || ins_or_w || ins_xor_w || ins_and_w || ins_sub_w || ins_sll_w
    || ins_srl_w || ins_sra_w || ins_slt_w || ins_sltu_w
    || ins_jal_w || ins_jalr_w
    || op_load_w;


  // Control Memory

  wire [31:0] dram_w_wdata = op_store_w ? rvfi_i_rs2_rdata : 0;
  wire        dram_w_wr    = op_store_w;
  wire [31:0] dram_w_addr =
    op_load_w  ? (rvfi_i_rs1_rdata + field_immi_w) :
    op_store_w ? (rvfi_i_rs1_rdata + field_imms_w) :
    0;
  wire [3:0] dram_w_bsel =
    ins_sw_w ? 4'b 1111 :
    ins_sh_w ? 4'b 0011 :
    ins_sb_w ? 4'b 0001 :
    4'd 0;


  // Calculate PC

  wire [31:0] pc_incr_w = rvfi_i_pc_rdata + 4;
  wire [31:0] pc_jump_w =
    ctrl_branch_w ? (rvfi_i_pc_rdata + field_immb_w)       :
    ins_jal_w     ? (rvfi_i_pc_rdata + field_immj_w)       :
    ins_jalr_w    ? ((rvfi_i_rs1_rdata + field_immi_w) & ~32'b1) :
    0;
  wire [31:0] pc_w_next =
    !rvfi_i_valid ? rvfi_i_pc_rdata :
    ctrl_jump_w   ? pc_jump_w :
    rvfi_i_valid  ? pc_incr_w :
    rvfi_i_pc_rdata;


  // RVFI

  reg [63:0] order = 0;

  always @(posedge sys_i_clk) begin
    order <=
      sys_i_rst    ? opt_i_defaultrvfiorder :
      rvfi_i_valid ? order + 1              :
      order;
  end

  assign rvfi_valid = sys_i_rst ? 1'd 0 : rvfi_i_valid;
  assign rvfi_order = order;
  assign rvfi_insn  = rvfi_i_insn;
  assign rvfi_halt  = 0;
  assign rvfi_intr  = 0;
  assign rvfi_mode  = 3;
  assign rvfi_ixl   = 1;
  assign rvfi_trap  =
    (ins_jal_w || ins_jalr_w || ctrl_branch_w)
    && (pc_w_next[1:0] != 0);

  assign rvfi_rd_addr   = rf_write_w ? field_rd_w : 5'd 0;
  assign rvfi_rs1_addr  = field_rs1_w;
  assign rvfi_rs2_addr  = field_rs2_w;
  assign rvfi_rd_wdata  = rf_wdata_w;
  assign rvfi_rs1_rdata = rvfi_i_rs1_rdata;
  assign rvfi_rs2_rdata = rvfi_i_rs2_rdata;

  assign rvfi_pc_rdata = rvfi_i_pc_rdata;
  assign rvfi_pc_wdata = pc_w_next;

  assign rvfi_mem_addr  = dram_w_addr;
  assign rvfi_mem_rmask =
    (ins_lb_w || ins_lbu_w) ? 4'b 0001 :
    (ins_lh_w || ins_lhu_w) ? 4'b 0011 :
    ins_lw_w                ? 4'b 1111 :
    4'd 0;
  assign rvfi_mem_wmask =
    op_store_w ?
      ins_sw_w ? 4'h F :
      ins_sh_w ? 4'h 3 :
      ins_sb_w ? 4'h 1 :
      4'd 0 :
    4'd 0;
  assign rvfi_mem_rdata = dram_i_rdata;
  assign rvfi_mem_wdata = dram_w_wdata;

  wire insgrp_w_immop =
    ins_addi_w | ins_slti_w | ins_sltiu_w | ins_xori_w | ins_ori_w
    | ins_andi_w | ins_lui_w | ins_auipc_w | ins_slli_w
    | ins_srli_w | ins_srai_w;
  wire insgrp_w_regreg =
    ins_add_w | ins_or_w | ins_xor_w | ins_and_w | ins_sub_w
    | ins_sll_w | ins_srl_w | ins_sra_w | ins_slt_w | ins_sltu_w;
  wire insgrp_w_loadstore =
    ins_lb_w | ins_lh_w | ins_lw_w | ins_lbu_w | ins_lhu_w | ins_sb_w
    | ins_sh_w | ins_sw_w;
  wire insgrp_w_ctrltrans =
    ins_beq_w | ins_bne_w | ins_blt_w | ins_bge_w | ins_bltu_w
    | ins_bgeu_w | ins_jal_w | ins_jalr_w;
  assign rvfi_cstm_unknowninstr =
    !(
      insgrp_w_immop | insgrp_w_regreg | insgrp_w_loadstore
      | insgrp_w_ctrltrans
    );

endmodule
